<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    // Agar nama tidak berubah menjadi jamak ex: Casts
    protected $table = "cast";

    // Memasukkan data-data yang hendak dimanipulasi dalam tabel
    protected $fillable = ['nama', 'umur', 'bio'];

    
}
