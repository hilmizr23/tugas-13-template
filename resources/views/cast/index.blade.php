@extends('layouts.master')

@section('title')
    List Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-success btn-sm my-2">Tambah</a>

<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <td>
                    {{$key + 1}}
                </td>
                <td>
                    {{$item->nama}}
                </td>
                <td>
                    {{$item->umur}}
                </td>
                <td>
                    {{$item->bio}}
                </td>
                <td>
                    <form class="form-inline" action="/cast/{{$item->id}}" method="POST">
                       @csrf
                       @method('delete')
                       <a href="/cast/{{$item->id}}" class="btn btn-primary btn-sm"> Detail </a>
                       <a href="/cast/{{$item->id}}/edit" class="btn btn-success btn-sm"> Edit </a>
                       <input type="submit" class="btn btn-danger btn-sm"value="Delete"> 
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>Tidak Ada Data</td>
            </tr>
        @endforelse
    </tbody>
  </table>
@endsection