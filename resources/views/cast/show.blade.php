@extends('layouts.master')

@section('title')
    Detail Cast : {{$cast->nama}}
@endsection

@section('content')

<h1>{{$cast->nama}}</h1>
<h3>{{$cast->umur}} Tahun </h3>
<p>{{$cast->bio}}</p>

@endsection